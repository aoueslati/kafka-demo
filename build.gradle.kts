plugins {
    java
    id("org.springframework.boot") version "3.1.3"
    id("idea")
}
apply(plugin = "io.spring.dependency-management")


tasks.jar {
    manifest {
        attributes(
            "api-Title" to "kafka-demo",
            "api-Version" to archiveVersion
        )
    }
}

repositories {
    mavenCentral()
    maven {
        url = uri("https://packages.confluent.io/maven")
    }
}


dependencies {

    implementation("org.springframework.boot:spring-boot-starter")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.kafka:spring-kafka:3.+")
    implementation("io.streamthoughts:azkarra-json-serializers:0.9.2")
    implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-avro:2.14.2")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.springframework.kafka:spring-kafka-test")
    annotationProcessor("org.springframework.boot:spring-boot-configuration-processor")
}

