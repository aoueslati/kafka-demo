package eu.ecomundo.kafka;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.config.SaslConfigs;
import org.apache.kafka.common.config.SslConfigs;
import org.apache.kafka.common.security.plain.PlainLoginModule;
import org.apache.kafka.common.security.scram.ScramLoginModule;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.core.RoutingKafkaTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;

import eu.ecomundo.kafka.properties.KafkaProperties;


/**
 * @author aoueslati
 */
@Configuration
@ComponentScan ({ "eu.ecomundo.kafka" })
@EnableScheduling
public class KafkaConfig {
	//@Bean


	@Bean
	public ProducerFactory<Object, Object> stringProducerFactory(KafkaProperties properties) throws IOException {
		Map<String, Object> configProps = new HashMap<>();
		configProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, properties.bootstrapServers());
		configProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		configProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		putSecurityProperties(configProps, properties.producer().username(), properties.producer().password());
		return new DefaultKafkaProducerFactory<>(configProps);
	}

	private static void putSecurityProperties(Map<String, Object> configProps, String username, String password) throws IOException {
		var cl = ResourceLoader.class.getClassLoader();
		ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(cl);
		Resource[] truststore = resolver.getResources("classpath:/truststore.jks");
		configProps.put("ssl.truststore.location", truststore[0].getFile().getAbsoluteFile().getAbsolutePath());
		configProps.put("ssl.truststore.password", "password");
//		Resource[] keystore = resolver.getResources("classpath:/client.keystore.jks");
//		configProps.put("ssl.keystore.location", keystore[0].getFile().getAbsoluteFile().getAbsolutePath());
//		configProps.put("ssl.keystore.password", "clientpass");
//		configProps.put(SslConfigs.SSL_ENDPOINT_IDENTIFICATION_ALGORITHM_CONFIG, "");
		configProps.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, "SASL_SSL");
		configProps.put(SaslConfigs.SASL_MECHANISM, "SCRAM-SHA-512");

		configProps.put(SaslConfigs.SASL_JAAS_CONFIG, String.format(
				"%s required username=\"%s\" " + "password=\"%s\";", ScramLoginModule.class.getName(), username, password
		));
	}



	@Bean
	public RoutingKafkaTemplate routingTemplate(ProducerFactory<Object, Object> stringProducerFactory, KafkaProperties properties) {
		Map<Pattern, ProducerFactory<Object, Object>> map = new LinkedHashMap<>();
		map.put(Pattern.compile(".+"), stringProducerFactory); // Default PF with StringSerializer
		return new RoutingKafkaTemplate(map);
	}

	@Bean
	public ConsumerFactory<String, String> consumerFactory(KafkaProperties properties) throws IOException {
		Map<String, Object> props = new HashMap<>();
		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, properties.bootstrapServers());
		//		props.put( ConsumerConfig.GROUP_ID_CONFIG, groupId);
		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
		props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, 500);
		props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);

		putSecurityProperties(props, properties.consumer().username(), properties.consumer().password());
		return new DefaultKafkaConsumerFactory<>(props);
	}


	@Bean
	public ConcurrentKafkaListenerContainerFactory<String, String> kafkaListenerContainerFactory(KafkaProperties properties) throws IOException {
		ConcurrentKafkaListenerContainerFactory<String, String> factory =
				new ConcurrentKafkaListenerContainerFactory<>();
		factory.setConsumerFactory(consumerFactory(properties));
		return factory;
	}
}
