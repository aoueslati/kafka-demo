package eu.ecomundo.kafka;

import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Main configuration class for the root context of an EcoMundo application.
 * <p>
 * This configuration class defines which Java property file will be loaded (using @PropertySource),
 * which packages will be scanned to discover Spring beans (using @ConfigurationPropertiesScan) and
 * will load sub-configuration classes (using @Import).
 *
 * @author aferrand
 */

@Configuration
@ConfigurationPropertiesScan ("eu.ecomundo.kafka.properties")
@Import ({
		WebConfig.class,
		KafkaConfig.class
})
public class RootConfig {
}
