package eu.ecomundo.kafka.properties;


import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties ("kafka")
//@ConstructorBinding
public record KafkaProperties(
		String bootstrapServers,
		ProducerConsumer consumer,
		ProducerConsumer producer
) {
	public record ProducerConsumer(String username, String password) {
	}
}
