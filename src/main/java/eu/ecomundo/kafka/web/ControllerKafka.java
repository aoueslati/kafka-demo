package eu.ecomundo.kafka.web;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.RoutingKafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author aoueslati
 */
@RestController
@RequestMapping ("/kafka/**")
public class ControllerKafka {

	private static final Logger LOGGER = LoggerFactory.getLogger(ControllerKafka.class);

	private final RoutingKafkaTemplate routingKafkaTemplate;

	private ConsumerFactory<String, String> consumerFactory;

	public ControllerKafka(RoutingKafkaTemplate routingKafkaTemplate, ConsumerFactory<String, String> consumerFactory) {
		this.routingKafkaTemplate = routingKafkaTemplate;
		this.consumerFactory = consumerFactory;
	}

	@GetMapping ("/kafka/push")
	public ResponseEntity<String> pushMessage(String topic, String key, String value) throws ExecutionException, InterruptedException {
		var res = routingKafkaTemplate.send(new ProducerRecord<>(topic, key, value));
		LOGGER.info("getProducerRecord = {}", res.get().getProducerRecord().value());
		LOGGER.info("getRecordMetadata = {}", res.get().getRecordMetadata());
		return ResponseEntity.ok("SUCCESS");
	}



	@GetMapping ("/kafka/consume/gr")
	public ResponseEntity<Collection<String>> consumeGr(String topic, String groupId) {
		KafkaConsumer<String, String> consumer = (KafkaConsumer<String, String>) consumerFactory.createConsumer(groupId, "client_id");
		consumer.subscribe(Collections.singletonList(topic));
		ConsumerRecords<String, String> records = consumer.poll(Duration.ofSeconds(5));
		Collection<String> messages = new ArrayList<>();
		for (ConsumerRecord<String, String> record : records) {
			messages.add(record.value());
		}
		consumer.commitAsync();
		consumer.close();
		return ResponseEntity.ok(messages);
	}



}

